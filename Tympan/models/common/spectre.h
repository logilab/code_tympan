/*
 * Copyright (C) <2012-2014> <EDF-R&D> <FRANCE>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef TY_MC_SPECTRE
#define TY_MC_SPECTRE

#include <vector>
#include <map>
#include <ostream>

#include "Tympan/core/macros.h"

/// Spectrum type.
enum TYSpectreType
{
    SPECTRE_TYPE_ATT,
    SPECTRE_TYPE_ABSO,
    SPECTRE_TYPE_LW,
    SPECTRE_TYPE_LP,
    SPECTRE_TYPE_AUTRE
};
/// Spectrum representation.
enum TYSpectreForm
{
    SPECTRE_FORM_TIERS,
    SPECTRE_FORM_OCT,
    SPECTRE_FORM_CST_DF,
    SPECTRE_FORM_UNSHAPED
};
/// Spectrum state (dB/Physical Measure).
enum TYSpectreEtat
{
    SPECTRE_ETAT_DB,
    SPECTRE_ETAT_LIN
};

/// Number of frequencies in third-octave bands
static const unsigned int TY_SPECTRE_DEFAULT_NB_ELMT = 31;

/// Number of frequencies in octave bands
static const unsigned int TY_SPECTRE_OCT_NB_ELMT = 9;

/// Default value for the spectrums.
static const double TY_SPECTRE_DEFAULT_VALUE = -100.0;

/// Frequencies collection
typedef std::vector<double> OTabFreq;

/**
 * Macro to check spectrum forms are consistent
 * and to return concrete instance to operate on.
 * Throws tympan::invalid_data if forms are inconsistent
 */
#define CHECK_FORM_AND_GET_CONCRETE_INSTANCE                                                                 \
                                                                                                             \
    if (this->getForm() != spectre.getForm())                                                                \
    {                                                                                                        \
        throw tympan::invalid_data("Spectrums must be same form") << tympan_source_loc;                      \
    }                                                                                                        \
    OSpectreAbstract* sp = getConcreteInstance();                                                            \
    OSpectreAbstract& s = *sp;

/**
 * \brief Store acoustic power values for different frequencies.
 *
 * @author Projet_Tympan
 *
 */
class OSpectreAbstract
{
    /// Methods
public:
    /// Constructors
    explicit OSpectreAbstract();
    explicit OSpectreAbstract(TYSpectreForm form);

    explicit OSpectreAbstract(bool isValid, TYSpectreType type, TYSpectreEtat etat, TYSpectreForm form);
    /// operator=
    virtual OSpectreAbstract& operator=(const OSpectreAbstract& other);
    /// operator==
    virtual bool operator==(const OSpectreAbstract& other) const;
    /// operator !=
    virtual bool operator!=(const OSpectreAbstract& other) const;

    /// Add a constant value to all the spectrum
    OSpectreAbstract& operator+(const double& valeur) const;

    /// Arithmetic sum of two spectrums in one-third Octave.
    OSpectreAbstract& operator+(const OSpectreAbstract& spectre) const;

    /// Arithmetic subtraction of two spectrums in one-third Octave.
    OSpectreAbstract& operator-(const OSpectreAbstract& spectre) const;

    /// Multiplication by a Spectre spectrum
    OSpectreAbstract& operator*(const OSpectreAbstract& spectre) const;
    /// Multiplication by a double coefficient
    OSpectreAbstract& operator*(const double& coefficient) const;

    /// Energetic sum of two spectrums.
    OSpectreAbstract& sumdB(const OSpectreAbstract& spectre) const;

    /// Get the array of real values - Pure virtual method
    virtual double* getTabValReel() = 0;
    virtual const double* getTabValReel() const = 0;

    /// Return an instance of a concrete class of the same type as current
    virtual OSpectreAbstract* getConcreteInstance() const = 0;

    /**
     * Initialize a spectrum to a default value.
     * @param valeur Value by default.
     */
    void setDefaultValue(const double& valeur = TY_SPECTRE_DEFAULT_VALUE);

    /**
     * \brief Check the spectrum validity.
     * Invalidity is caused by: corrupted data, impossible calculation.
     *
     * @return Spectrum state (valid = true / non valid = false).
     */
    bool isValid() const
    {
        return _valid;
    }

    /**
     * Force the validity state of the spectrum.
     */
    void setValid(const bool& valid = true)
    {
        _valid = valid;
    }

    /// Get the spectrum type.
    TYSpectreType getType() const
    {
        return _type;
    }
    /// Set the spectrum type.
    void setType(TYSpectreType type)
    {
        _type = type;
    }

    /// Get the spectrum state.
    TYSpectreEtat getEtat() const
    {
        return _etat;
    };
    const TYSpectreEtat getEtat()
    {
        return _etat;
    };

    /// Force the spectrum state (to use carefully ...)
    void setEtat(TYSpectreEtat etat)
    {
        _etat = etat;
    }

    /// Get the spectrum form.
    TYSpectreForm getForm() const
    {
        return _form;
    };

    /// Force the spectrum state (to use carefully ...)
    void setForm(TYSpectreForm form)
    {
        _form = form;
    }

    /// Number of values in the spectrum
    unsigned int getNbValues() const;

    void setDefaultValue(double module[], const unsigned int spectreNbElmt = TY_SPECTRE_DEFAULT_NB_ELMT,
                         const double& valeur = TY_SPECTRE_DEFAULT_VALUE);

    /**
     * Get an interval of real values from the frequencies/complex array
     *
     * @param[out] valeurs Values array. It should be allocated before.
     * @param[in] nbVal Number of values to retrieve from the spectrum
     * @param[in] decalage Index of the first value to retrieve
     */
    void getRangeValueReal(double* valeurs, const short& nbVal, const short& decalage);

    /// Arithmetic sum of two spectrums in one-third Octave.
    OSpectreAbstract& sum(const OSpectreAbstract& spectre) const;

    /// Arithmetic sum of a spectrum and a double scalar.
    OSpectreAbstract& sum(const double& value) const;

    /// Multiplication of two spectrums.
    OSpectreAbstract& mult(const OSpectreAbstract& spectre) const;

    /// Multiply this spectrum par a double scalar.
    OSpectreAbstract& mult(const double& coefficient) const;

    /// Division of two spectrums.
    OSpectreAbstract& div(const OSpectreAbstract& spectre) const;

    /// Division of this spectrum by a double scalar.
    OSpectreAbstract& div(const double& coefficient) const;

    /// Arithmetic subtraction of two spectrums in one-third Octave.
    OSpectreAbstract& subst(const OSpectreAbstract& spectre) const;

    /// Subtract a constant value to this spectrum
    OSpectreAbstract& subst(const double& valeur) const;

    /// Division of a double constant by this spectrum.
    OSpectreAbstract& invMult(const double& coefficient = 1.0) const;

    /// Division of one by this spectrum.
    OSpectreAbstract& inv() const;

    /// Return a spectrum as this spectrum raised to a double power.
    OSpectreAbstract& power(const double& puissance) const;

    /// Compute the log base n of this spectrum (n=10 by default).
    OSpectreAbstract& log(const double& base = 10.0) const;

    /// Compute the root square of this spectrum.
    OSpectreAbstract& racine() const;

    /// Compute e^(coef * spectre) of this spectrum.
    OSpectreAbstract& exp(const double coef = 1.0);

    /// Compute the sin of this spectrum
    OSpectreAbstract& sin() const;

    /// Compute the cos of this spectrum
    OSpectreAbstract& cos() const;

    /// Return the absolute value of this spectrum
    OSpectreAbstract& abs() const;

    /**
     * \fn OSpectreAbstract& sqrt() const
     * \brief Return the root square of a spectrum
     */
    OSpectreAbstract& sqrt() const;

    /**
     * \fn double valMax();
     * \brief Return the maximum value of a spectrum
     */
    double valMax();

    /// Limit the spectrum values (min and max)
    OSpectreAbstract& seuillage(const double& min = -200.0, const double max = 200.0);

    /// Sum the values of the spectrum
    double sigma();
    const double sigma() const;

    /// Converts to dB.
    OSpectreAbstract& toDB() const;

    /// Converts to physical quantity.
    OSpectreAbstract& toGPhy() const;

    /// Compute the global value dB[Lin] of a one-third Octave spectrum.
    double valGlobDBLin() const;

    /// Compute the global value dB[A] of a one-third Octave spectrum.
    double valGlobDBA() const;

    /// Converts to one-third Octave.
    OSpectreAbstract& toTOct() const;

    /// Converts to Octave.
    OSpectreAbstract& toOct() const;

    /// Rounds the spectrum values to two digits.
    ///
    /// This method is used to round Lw spectrum values to avoid precision issues when testing 17534-3
    /// conformity.
    OSpectreAbstract& round();

    /// Print the spectrum
    virtual void printme() const;

    // === STATIC FUNCTIONS

    /// Make a spectrum in Octave.
    static OSpectreAbstract& makeOctSpect();

    // Members
public:
protected:
    // ==== Static members
    /// Default value for the spectrum
    static double _defaultValue;

    // ==== Other members

    /// Spectrum validity
    bool _valid;

    /// Spectrum type
    TYSpectreType _type;

    /// Spectrum state (physical quantity or dB).
    TYSpectreEtat _etat;

    /// Representation of the spectrum: one-third Octave, Octave, constant delta f, unstructured.
    TYSpectreForm _form;
};

class OSpectre : public OSpectreAbstract
{
    // Methods
public:
    /// Default constructor, the spectrum module is defined by the _defaultValue
    OSpectre();
    /// Constructor, the spectrum module is defined by the defaultValue given in parameter
    OSpectre(double defaultValue);
    /// Copy constructor
    OSpectre(const OSpectre& other);
    /// Copy constructor from OSpectreAbstract
    OSpectre(const OSpectreAbstract& other);

    /**
     * \brief Constructor from an array (needed for the Harmonoise model)
     * \param valeurs Array of values per frequency
     * \param nbVal  Array size
     * \param decalage Frequency offset relative to the standard band TYMPAN (16-16000)
     */
    OSpectre(const double* valeurs, unsigned nbVal, unsigned decalage);
    /// Destructor
    virtual ~OSpectre();
    /// operator=
    OSpectre& operator=(const OSpectre& other);
    OSpectreAbstract& operator=(const OSpectreAbstract& other) override;
    /// operator==
    virtual bool operator==(const OSpectre& other) const;
    /// operator !=
    virtual bool operator!=(const OSpectre& other) const;

    /// XXX These are the modulus to put into the solver model.
    /// Get the array of real values
    double* getTabValReel() override
    {
        return _module;
    }
    const double* getTabValReel() const override
    {
        return _module;
    }

    /// Return an instance of a concrete class of the same type as current
    OSpectreAbstract* getConcreteInstance() const override;

    /**
     * Set a new value to the frequencies array.
     *
     * @param freq Frequency to which a new value is given.
     * @param reel Value.
     */
    void setValue(const double& freq, const double& reel = 0.0);

    /**
     * Get the value for a frequency.
     *
     * @param freq Frequency.
     *
     * @return The real number.
     */
    double getValueReal(double freq);

    /// Existence d'une tonalite marquee
    bool isTonalite() const;

    // === STATIC FUNCTIONS

    /// Create a physical quantity spectrum
    static OSpectre getEmptyLinSpectre(const double& valInit = 1.0E-20);

    /// Make a spectrum in Octave.
    static OSpectre makeOctSpect();

    /// Return the index associated to a frequency.
    static int getIndice(const double& freq)
    {
        return _mapFreqIndice[freq];
    }

    /// Define minimal frequency
    static void setFMin(const double& fMin)
    {
        _fMin = fMin;
    }
    /// Define maximal frequency
    static void setFMax(const double& fMax)
    {
        _fMax = fMax;
    }

    /**
     * \fn OTabFreq getTabFreqExact()
     * Return the array of exact frequencies.
     * The calling method should free the allocated memory for this array.
     *
     * \return The array of exact frequencies.
     */
    static OTabFreq getTabFreqExact(); // XXX These are the frequencies to use in solver

    /**
     * \fn OSpectre getOSpectreFreqExact()
     * \brief Return the spectrum of the exact frequencies.
     * \return An OSpectre
     */
    static OSpectre getOSpectreFreqExact();

    /// Construction du tableau frequence/indice
    static std::map<double, int> setMapFreqIndice();

    /// Build a weighted spectrum A.
    static OSpectre pondA();

    /// Build a weighted spectrum B.
    static OSpectre pondB();

    /// Build a weighted spectrum C
    static OSpectre pondC();

    /**
     * Return a spectrum representating the wavelength associated to each frequency
     * @param c Wave propagating speed
     * @return An OSpectre
     */
    static OSpectre getLambda(const double& c);

    // Members
public:
protected:
    // ==== Static members
    // CAUTION Check how those static members behave in shared libraries

    /// Array of center frequencies (Hz) normalized in one-third Octave
    static const double _freqNorm[];

    /// Minimal frequency
    static double _fMin;

    /// Maximal frequency
    static double _fMax;

    /// Mapping between frequency and array index
    static std::map<double, int> _mapFreqIndice;

    /// Real values array for module
    double _module[TY_SPECTRE_DEFAULT_NB_ELMT];
};

::std::ostream& operator<<(::std::ostream& os, const OSpectre& s);

typedef std::vector<OSpectre> OTabSpectre;                //!< Spectrums vector
typedef std::vector<std::vector<OSpectre>> OTab2DSpectre; //!< Spectrums 2D array

/**
 * Define a module/phase spectrum
 *
 */
class OSpectreComplex : public OSpectre
{
    // Methods
public:
    OSpectreComplex();
    OSpectreComplex(const TYComplex& defaultValue);
    OSpectreComplex(const OSpectreComplex& other);
    /**
     * Constructor from a OSpectre (which is the module of the complex spectrum)
     */
    OSpectreComplex(const OSpectre& other);
    /**
     * Constructor from 2 OSpectre : module and phase
     */
    OSpectreComplex(const OSpectre& spectre1, const OSpectre& spectre2);

    virtual ~OSpectreComplex();
    /// operators
    OSpectreComplex& operator=(const OSpectreComplex& other);
    OSpectreAbstract& operator=(const OSpectreAbstract& other) override;

    bool operator==(const OSpectreComplex& other) const;
    bool operator!=(const OSpectreComplex& other) const;
    OSpectreComplex operator+(const OSpectreComplex& spectre) const;
    /// Product of two complex spectrums (module/phase)
    OSpectreComplex operator*(const OSpectreComplex& spectre) const;
    /// Computes the operation between a complex spectrum times a spectrum
    OSpectreComplex operator*(const OSpectre& spectre) const;
    /// Multiply a  complex spectrum by a double coefficient
    OSpectreComplex operator*(const double& coefficient) const;
    /// Divide a complex spectrum by another one
    OSpectreComplex operator/(const OSpectreComplex& spectre) const;

    /// Get an array of the imaginary values of the spectrum
    double* getTabValImag()
    {
        return _phase;
    }
    const double* getTabValImag() const
    {
        return _phase;
    }

    /**
     * Set a complex value to the array Frequency/Complex.
     *
     * @param freq Frequency.
     * @param reel Real number.
     * @param imag Imaginary number.
     */
    void setValue(const double& freq, const double& reel, const double& imag = 0.0);
    /**
     * Set a complex value to the array Frequency/Complex.
     *
     * @param freq Frequency.
     * @param cplx A TYComplex complex number
     */
    void setValue(const double& freq, const TYComplex& cplx);
    /**
     * Get for a frequency the imaginary number from the Frequency/Complex array
     *
     * @param freq Frequency.
     * @param pValid False if the frequency is not valid for this spectrum.
     *
     * @return Imaginary number
     */
    double getValueImag(double freq, bool* pValid = 0);
    /**
     * Set the phase to a spectrum.
     * @param spectre A OSpectre.
     */
    void setPhase(const OSpectre& spectre);
    /**
     * Set the phase to this spectrum.
     * @param valeur A double.
     */
    void setPhase(const double& valeur = 0.0);
    /**
     * Get the phase of this spectrum.
     * @return A OSpectre.
     */
    OSpectre getPhase() const;
    /**
     * Get the module of this spectrum.
     */
    OSpectre getModule() const;

    /// Conversion to module/phase
    OSpectreComplex toModulePhase() const;

    // === STATIC FUNCTIONS

    /// Build a OSpectreComplex complex spectrum
    static OSpectreComplex getCplxSpectre(const double& valInit = 1.0E-20);
    // Members
public:
protected:
    /// Array of the imaginary numbers (phase)
    double _phase[TY_SPECTRE_DEFAULT_NB_ELMT];
};

typedef std::vector<OSpectreComplex> OTabSpectreComplex;                //!< OTabSpectreComplex vector
typedef std::vector<std::vector<OSpectreComplex>> OTab2DSpectreComplex; //!< OTabSpectreComplex 2D array

/**
 * Define an octave band spectrum
 *
 */
class OSpectreOctave : public OSpectreAbstract
{
    // Methods
public:
    OSpectreOctave();
    OSpectreOctave(bool isValid, TYSpectreType type, TYSpectreEtat etat);
    OSpectreOctave(double defaultValue);
    OSpectreOctave(const double* valeurs, unsigned nbVal, unsigned decalage);
    OSpectreOctave(const OSpectreOctave& other);
    /// Copy constructor from OSpectreAbstract
    OSpectreOctave(const OSpectreAbstract& other);

    /**
     * Constructor from a OSpectre
     */
    OSpectreOctave(const OSpectre& other);

    virtual ~OSpectreOctave();

    /// operators
    OSpectreOctave& operator=(const OSpectreOctave& other);
    OSpectreAbstract& operator=(const OSpectreAbstract& other) override;
    bool operator==(const OSpectreOctave& other) const;
    bool operator!=(const OSpectreOctave& other) const;

    /// Get an array of the real values of the spectrum
    double* getTabValReel() override
    {
        return _module;
    }

    const double* getTabValReel() const override
    {
        return _module;
    }

    /// Return an instance of a concrete class of the same type as current
    OSpectreAbstract* getConcreteInstance() const override;

    /**
     * Set a new value to the frequencies array.
     *
     * @param freq Frequency to which a new value is given.
     * @param reel Value.
     */
    void setValue(const double& freq, const double& reel = 0.0);

    // === STATIC FUNCTIONS

    /// Create a physical quantity spectrum
    static OSpectreOctave getEmptyLinSpectre(const double& valInit = 1.0E-20);

    /**
     * \fn OTabFreq getTabFreqExact()
     * Return the array of exact frequencies.
     * The calling method should free the allocated memory for this array.
     *
     * \return The array of exact frequencies.
     */
    static OTabFreq getTabFreqExact(); // XXX These are the frequencies to use in solver

    /// Build a weighted spectrum A.
    static OSpectreOctave pondA();

    /**
     * Return a spectrum representating the wavelength associated to each frequency
     * @param c Wave propagating speed
     * @return An OSpectreOctave
     */
    static OSpectreOctave getLambda(const double& c);

    /// Build frequency/index map
    static std::map<double, int> setMapFreqIndice();

    // Members
public:
protected:
    // ==== Static members
    /// Mapping between frequency and array index
    static std::map<double, int> _mapFreqIndice;

    /// Array of center frequencies (Hz) normalized in one-third Octave
    static const double _freqNorm[TY_SPECTRE_OCT_NB_ELMT];

    /// A weighting for ponderation A computation
    static const double _AWeighting[TY_SPECTRE_OCT_NB_ELMT];

    // ==== Other members

    /// Real values array for module
    double _module[TY_SPECTRE_OCT_NB_ELMT];
};

#endif // TY_MC_SPECTRE
