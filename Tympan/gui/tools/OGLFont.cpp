/*
 * Copyright (C) <2012> <EDF-R&D> <FRANCE>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/*
 *
 */

#include <stdio.h>
#include "Tympan/core/color.h"
#include "OGLFont.h"
#include "OImageFont.h"
#include "TYImageManager.h"
#include <iostream>
OGLFont::OGLFont() : _image(0) {}

OGLFont::~OGLFont() {}

bool OGLFont::load(const char* filename)
{
    if (id > 0)
    {
        free();
    }

    genTexture();

    glBindTexture(GL_TEXTURE_2D, id);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

    // Recover the image
    _image = (OImageFont*)TYImageManager::get()->getImage(filename);

    if (!_image)
    {
        return false;
    }

    // Recover the texture's dimensions
    GLsizei w = _image->getSizeX();
    GLsizei h = _image->getSizeY();

    unsigned int bpp = _image->getDepth() / 8;

    GLenum format = GL_ALPHA;

    // Create the texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, w, h, 0, GL_ALPHA, GL_UNSIGNED_BYTE, _image->getData());

    return true;
}

void OGLFont::drawText(const std::string& msg, const OColor& color, double x, double y) const
{
    if (!_image)
    {
        return;
    }

    // Texts must always be displayed in GL_FILL mode
    GLdouble polygonMode[2];
    glGetDoublev(GL_POLYGON_MODE, polygonMode);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    glDisable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);

    // Text color
    glColor3fv(color);

    // Draw a quad
    glBegin(GL_QUADS);

    // Recover the texture's dimensions
    size_t width = _image->getSizeX();
    size_t height = _image->getSizeY();

    // Process each character of msg
    for (unsigned int i = 0; i < msg.length(); ++i)
    {
        OImageFont::OGLFontChar c = _image->getChar(msg[i]);

        // Compute the relative coordinates of the char on the texture
        double x0 = static_cast<double>(c.x) / width;
        double y0 = static_cast<double>(c.y) / height;

        double x1 = static_cast<double>(c.x + c.w) / width;
        double y1 = static_cast<double>(c.y + c.h) / height;

        // Map the piece of texture corresponding to the current char to its position
        glTexCoord2d(x0, y0);
        glVertex2d(x, y);
        glTexCoord2d(x0, y1);
        glVertex2d(x, y - c.h);
        glTexCoord2d(x1, y1);
        glVertex2d(x + c.w, y - c.h);
        glTexCoord2d(x1, y0);
        glVertex2d(x + c.w, y);

        // Apply the advance to the x position
        x += c.advance;

        // Apply the kerning between the current character and next one
        if (i < msg.length() - 1)
            x += _image->getKerning(msg[i], msg[i + 1]);
    }

    // End quad
    glEnd();

    glDisable(GL_TEXTURE_2D);
    glEnable(GL_DEPTH_TEST);

    glPolygonMode(GL_FRONT_AND_BACK, (GLenum)polygonMode[0]);
}
