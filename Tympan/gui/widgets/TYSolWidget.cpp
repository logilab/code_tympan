/*
 * Copyright (C) <2012> <EDF-R&D> <FRANCE>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * \file TYSolWidget.cpp
 * \brief outil IHM pour un sol
 */

// Added by qt3to4:
#include <QGridLayout>
#include <QLabel>

#include "Tympan/models/business/OLocalizator.h"
#include "Tympan/models/business/material/TYSol.h"
#include "TYSolWidget.h"
#include "Tympan/gui/widgets/TYSolResistanceDialog.h"
#include "Tympan/gui/widgets/TYLineEdit.h"

#define TR(id) OLocalizator::getString("TYSolWidget", (id))

TYSolWidget::TYSolWidget(TYSol* pElement, QWidget* _pParent /*=NULL*/) : TYWidget(pElement, _pParent)
{
    _elmW = new TYElementWidget(pElement, this);

    resize(300, 300);
    setWindowTitle(TR("id_caption"));
    _solLayout = new QGridLayout();
    setLayout(_solLayout);

    _solLayout->addWidget(_elmW, 0, 0);

    _groupBoxDefaultSolver = new QGroupBox(this);
    _groupBoxDefaultSolver->setTitle("DefaultSolver");
    _groupBoxDefaultSolverLayout = new QGridLayout();
    _groupBoxDefaultSolver->setLayout(_groupBoxDefaultSolverLayout);
    _groupBoxDefaultSolverLayout->setColumnStretch(2, 1);

    _lineEditEpaisseur = new TYLineEdit(QString(), false, false, _groupBoxDefaultSolver);
    _groupBoxDefaultSolverLayout->addWidget(_lineEditEpaisseur, 1, 1);
    QLabel* pUnitEpais = new QLabel(_groupBoxDefaultSolver);
    pUnitEpais->setText(TR("id_unite_epaisseur"));
    _groupBoxDefaultSolverLayout->addWidget(pUnitEpais, 1, 2);

    _lineEditResistivite = new TYLineEdit(QString(), false, false, _groupBoxDefaultSolver);
    _groupBoxDefaultSolverLayout->addWidget(_lineEditResistivite, 0, 1);
    QLabel* pUnitResis = new QLabel(_groupBoxDefaultSolver);
    pUnitResis->setText(TR("id_unite_resistivite"));
    _groupBoxDefaultSolverLayout->addWidget(pUnitResis, 0, 2);

    _lineEditEcartType = new TYLineEdit(_groupBoxDefaultSolver);
    _groupBoxDefaultSolverLayout->addWidget(_lineEditEcartType, 2, 1);
    QLabel* pUnitEcart = new QLabel(_groupBoxDefaultSolver);
    pUnitEcart->setText(TR("id_unite_ecarttype"));
    _lineEditEcartType->setEnabled(false);
    _groupBoxDefaultSolverLayout->addWidget(pUnitEcart, 2, 2);

    _lineEditLongueur = new TYLineEdit(QString(), false, false, _groupBoxDefaultSolver);
    _groupBoxDefaultSolverLayout->addWidget(_lineEditLongueur, 3, 1);
    QLabel* pUnitLong = new QLabel(_groupBoxDefaultSolver);
    pUnitLong->setText(TR("id_unite_longueur"));
    _lineEditLongueur->setEnabled(false);
    _groupBoxDefaultSolverLayout->addWidget(pUnitLong, 3, 2);

    _pushButtonResistivite = new QPushButton(_groupBoxDefaultSolver);
    _pushButtonResistivite->setText(TR("id_edit_resistivite"));
    _groupBoxDefaultSolverLayout->addWidget(_pushButtonResistivite, 0, 3);

    _labelEpaisseur = new QLabel(_groupBoxDefaultSolver);
    _labelEpaisseur->setText(TR("id_epaisseur_label"));
    _groupBoxDefaultSolverLayout->addWidget(_labelEpaisseur, 1, 0);

    _labelResistivite = new QLabel(_groupBoxDefaultSolver);
    _labelResistivite->setText(TR("id_resistivite_label"));
    _groupBoxDefaultSolverLayout->addWidget(_labelResistivite, 0, 0);

    _labelEcartType = new QLabel(_groupBoxDefaultSolver);
    _labelEcartType->setText(TR("id_ecarttype_label"));
    _groupBoxDefaultSolverLayout->addWidget(_labelEcartType, 2, 0);

    _labelLongueur = new QLabel(_groupBoxDefaultSolver);
    _labelLongueur->setText(TR("id_longueur_label"));
    _groupBoxDefaultSolverLayout->addWidget(_labelLongueur, 3, 0);

    _solLayout->addWidget(_groupBoxDefaultSolver, 1, 0);

    _groupBox9613Solver = new QGroupBox(this);
    _groupBox9613Solver->setTitle("ISO 9613");
    _groupBox9613SolverLayout = new QGridLayout();
    _groupBox9613Solver->setLayout(_groupBox9613SolverLayout);
    _groupBox9613SolverLayout->setColumnStretch(2, 1);

    _lineEditFacteurG = new TYLineEdit(_groupBox9613Solver);
    _groupBox9613SolverLayout->addWidget(_lineEditFacteurG, 0, 1);
    QLabel* pUnitG = new QLabel(_groupBox9613Solver);
    pUnitG->setText("-");
    _groupBox9613SolverLayout->addWidget(pUnitG, 0, 2);

    _labelFacteurG = new QLabel(_groupBox9613Solver);
    _labelFacteurG->setText(TR("id_facteur_g_label"));
    _groupBox9613SolverLayout->addWidget(_labelFacteurG, 0, 0);

    _solLayout->addWidget(_groupBox9613Solver, 2, 0);

    connect(_pushButtonResistivite, &QPushButton::clicked, this, &TYSolWidget::editResistivite);

    updateContent();
}

TYSolWidget::~TYSolWidget() {}

void TYSolWidget::updateContent()
{
    QString num;

    _elmW->updateContent();

    _lineEditResistivite->setText(num.setNum(getElement()->getResistivite(), 'f', 2));
    _lineEditEpaisseur->setText(num.setNum(getElement()->getEpaisseur(), 'f', 2));
    _lineEditEcartType->setText(num.setNum(getElement()->getEcartType(), 'f', 2));
    _lineEditLongueur->setText(num.setNum(getElement()->getLongueur(), 'f', 3));

    _lineEditFacteurG->setText(num.setNum(getElement()->getG(), 'f', 2));
}

void TYSolWidget::apply()
{
    _elmW->apply();

    // On ne peut pas rentrer une resistivite <= 0 (mini 1 kRayl)
    double resistivite = _lineEditResistivite->text().toDouble();
    resistivite = resistivite <= 1.0 ? 1.0 : resistivite;
    getElement()->setResistivite(resistivite);

    // On ne peut avoir une epaisseur <= 0 (mini 1 cm)
    double epaisseur = _lineEditEpaisseur->text().toDouble();
    epaisseur = epaisseur <= 0.01 ? 0.01 : epaisseur;
    getElement()->setEpaisseur(epaisseur);

    double ecarttype = _lineEditEcartType->text().toDouble();
    ecarttype = ecarttype < 0 ? 0 : ecarttype;
    getElement()->setEcartType(ecarttype);

    double longueur = _lineEditLongueur->text().toDouble();
    longueur = longueur <= 0 ? 0.0001 : longueur;
    getElement()->setLongueur(longueur);

    // G factor must be between 0 and 1
    double G = _lineEditFacteurG->text().toDouble();
    G = G < 0.00 ? 0.00 : (G > 1.00 ? 1.00 : G);
    getElement()->setG(G);

    emit modified();
}

void TYSolWidget::editResistivite()
{
    TYSolResistanceDialog* pDialog = new TYSolResistanceDialog(this, getElement()->getResistivite());

    int ret = pDialog->exec();

    if (ret == QDialog::Accepted)
    {
        // Mise a jour du widget associe a la resistivite
        _lineEditResistivite->setText(QString().setNum(pDialog->getResistivite(), 'f', 2));
    }
}
