/*
 * Copyright (C) <2012> <EDF-R&D> <FRANCE>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * \file TYInputDialog.h
 * \brief outil IHM pour une entrée utilisateur (fichier header)
 * \author Projet_Tympan
 *
 */

#ifndef __TY_LINE_EDIT__
#define __TY_LINE_EDIT__

#include <QInputDialog>
#include <QRegExpValidator>

class TYInputDialog : public QInputDialog
{
    Q_OBJECT
public:
    /**
     *  Constructor
     */
    TYInputDialog(bool isZeroOK = true, QWidget* parent = nullptr, Qt::WindowFlags flags = Qt::WindowFlags());

    /**
     * Connect the signal 'accepted' to the slot 'adjustDecimal'
     */
    void connect();

    /**
     * Overload of the QInputDialog method
     * Call adjustTextColor if the event is keyRelease
     */
    bool eventFilter(QObject* object, QEvent* event) override;

public slots:
    /**
     * If the text is not a double with at most 2 decimal numbers, it is colored red
     */
    void adjustTextColor();

    /**
     * If the text include a coma, it is changed into a point
     */
    void adjustDecimal();
};

#endif