/*
 * Copyright (C) <2012> <EDF-R&D> <FRANCE>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * \file TYSolverParamsWidgetManager.h
 * \brief Objet permettant de gerer les differents widgets servant a parametriser le solveur
 * \author Projet_Tympan
 *
 */

#ifndef TYSOLVERPARAMSWIDGETMANAGER_H
#define TYSOLVERPARAMSWIDGETMANAGER_H

#include <QString>
#include <QMap>

#include "TYSolverParamsWidget.h"

class TYSolverParamsWidgetManager : public QObject
{
    Q_OBJECT

public:
    TYSolverParamsWidgetManager();
    TYSolverParamsWidgetManager(QString dataModelPath);

    void readDataModel(QString dataModelPath);
    TYSolverParamsDataModel* getDataModel(QString paramName);
    TYSolverParamsWidget* getWidget(QString paramName);
    TYSolverParamsWidget* makeWidget(QString paramName);
    TYSolverParamsWidget* makeWidget(TYSolverParamsDataModel* dataModel);
    void registerWidget(TYSolverParamsWidget* widget);
    void updateWidgets(QString newParamValues);
    QString getSolverParams()
    {
        return _solverParams;
    }
    void setSolverParams(QString newSolverParams)
    {
        _solverParams = newSolverParams;
    }
    void forceSyncParams();

private:
    QMap<QString, TYSolverParamsDataModel*> _dataModelMap;
    QMap<QString, TYSolverParamsWidget*> _solverParamsWidgets;
    QString _solverParams;

public slots:
    void widgetEdited(QString newValue);
};

#endif // TYSOLVERPARAMSWIDGETMANAGER_H
