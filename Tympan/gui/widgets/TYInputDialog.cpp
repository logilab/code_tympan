/*
 * Copyright (C) <2012> <EDF-R&D> <FRANCE>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * \file TYLineEdit.h
 * \brief outil IHM pour une entrée utilisateur (fichier header)
 * \author Projet_Tympan
 *
 */

#include <QKeyEvent>
#include <QWidget>
#include <QApplication>

#include "TYInputDialog.h"

TYInputDialog::TYInputDialog(bool isZeroOK, QWidget* parent, Qt::WindowFlags flags)
    : QInputDialog(parent, flags)
{
    connect();
    installEventFilter(this);
}

bool TYInputDialog::eventFilter(QObject* object, QEvent* event)
{
    if (event->type() == QEvent::KeyRelease)
    {
        adjustTextColor();
    }
    return QInputDialog::eventFilter(object, event);
}

void TYInputDialog::connect()
{
    QObject::connect(this, &QDialog::accepted, this, &TYInputDialog::adjustDecimal);
}

void TYInputDialog::adjustDecimal()
{
    QString txt = textValue();
    txt.replace(QRegExp(","), ".");
    setTextValue(txt);
    setDoubleValue(txt.toDouble());
}

void TYInputDialog::adjustTextColor()
{
    QRegExp regex("[-]{0,1}[0-9]{1,}[,|.]{0,1}[0-9]{0,2}");

    if (!regex.exactMatch(textValue()))
    {
        this->setStyleSheet("QLineEdit { color: red;}");
    }
    else
    {
        this->setStyleSheet("QLineEdit { color: black;}");
    }
}