/*
 * Copyright (C) <2012> <EDF-R&D> <FRANCE>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef __TY_SOLVER_HELPER_9613__
#define __TY_SOLVER_HELPER_9613__

#include <models/common/spectre.h>
#include <solvers/9613Solver/TYTrajet.h>

struct PathResults
{
    int path_id;
    TYTypeChemin pathType;
    OSpectreOctave LW;
    OSpectreOctave Dc;
    OSpectreOctave Aatm;
    OSpectreOctave Agr_s;
    OSpectreOctave Agr_r;
    OSpectreOctave Agr_m;
    OSpectreOctave Dz_top;
    OSpectreOctave Dz_left;
    OSpectreOctave Dz_right;
    OSpectreOctave Abar_top;
    OSpectreOctave Abar_left;
    OSpectreOctave Abar_right;
    OSpectreOctave Abar;
    OSpectreOctave Adiv;
    OSpectreOctave A;
    OSpectreOctave L;
};

class TYSolverHelper
{
    // === STATIC FUNCTIONS
public:
    /**
     * \fn void exportResults17534()
     * Trace intermediate solver results in 17534 format
     *
     */
    static void exportResults17534(const std::vector<PathResults> pathsResults, const OSpectreOctave& SLp,
                                   const AtmosphericConditions& atmos);

    static void printQuantity(std::ofstream& ofs, const OSpectreOctave Att, const char* quantity);

    static void printLevel(std::ofstream& ofs, const OSpectreOctave L, const char* quantity);
};

#endif // __TY_SOLVER_HELPER_9613__
