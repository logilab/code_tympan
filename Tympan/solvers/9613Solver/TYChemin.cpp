/*
 * Copyright (C) <2012> <EDF-R&D> <FRANCE>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "TYChemin.h"
#include "Tympan/models/solver/config.h"

TYChemin::TYChemin() : _typeChemin(TYTypeChemin::CHEMIN_DIRECT), _longueur(0.0), _distance(0.0)
{
    _attenuations.clear();
    _eq_path = new acoustic_path();
}

TYChemin::TYChemin(const TYChemin& other)
{
    *this = other;
}

TYChemin::~TYChemin()
{
    if (tympan::SolverConfiguration::get()->KeepRays == false && _eq_path != nullptr)
    {
        _eq_path->cleanEventsTab();
    }
    _attenuations.clear();
}

TYChemin& TYChemin::operator=(const TYChemin& other)
{
    if (this != &other)
    {
        _typeChemin = other._typeChemin;
        _distance = other._distance;
        _longueur = other._longueur;
        _eq_path = other._eq_path;
        _attenuations = other._attenuations;
    }

    return *this;
}

bool TYChemin::operator==(const TYChemin& other) const
{
    if (this != &other)
    {
        if (_typeChemin != other._typeChemin)
        {
            return false;
        }
        if (_distance != other._distance)
        {
            return false;
        }
        if (_longueur != other._longueur)
        {
            return false;
        }
        if (_eq_path != other._eq_path)
        {
            return false;
        };
        if (_attenuations != other._attenuations)
        {
            return false;
        }
    }

    return true;
}

bool TYChemin::operator!=(const TYChemin& other) const
{
    return !operator==(other);
}

void TYChemin::calcAttenuation(const TYTabEtape& tabEtapes, const AtmosphericConditions& atmos, double dp,
                               double hs, double hr, double Gs, double Gm, double Gr)
{
    unsigned int i = 0;

    switch (_typeChemin)
    {
        case TYTypeChemin::CHEMIN_DIRECT:
            _attenuations[TYTypeAttenuation::DIRECTIVITY_INDEX] =
                tabEtapes[0]._Absorption; // S = Source directivity index
            _attenuations[TYTypeAttenuation::ATTENUATION_ATM] =
                atmos.compute_length_absorption_oct(_longueur); // Aatm = alpha * d
            calcGroundAttenuations(dp, hs, hr, Gs, Gm, Gr);
            break;

        case TYTypeChemin::CHEMIN_REFLEX:
            _attenuations[TYTypeAttenuation::ATTENUATION_ATM] =
                atmos.compute_length_absorption_oct(_longueur); // Aatm = alpha * d
            calcGroundAttenuations(dp, hs, hr, Gs, Gm, Gr);
            _attenuations[TYTypeAttenuation::DIRECTIVITY_INDEX] =
                tabEtapes[0]._Absorption; // S = Source directivity index
            // We multiply steps absorptions from second
            _attenuations[TYTypeAttenuation::ATTENUATION_REFLEX] = OSpectreOctave(1.0);
            for (i = 1; i < tabEtapes.size(); i++)
            {
                _attenuations[TYTypeAttenuation::ATTENUATION_REFLEX] =
                    _attenuations[TYTypeAttenuation::ATTENUATION_REFLEX] *
                    (OSpectreOctave(1.0) -
                     tabEtapes[i]._Absorption); // Product of directivty factor and reflexion coefficients
            }
            break;

        default:
            break;
    }

    build_eq_path(tabEtapes);
}

void TYChemin::computeBarAttenuation(const OSpectreOctave& Dz, const bool vertical, const bool left)
{
    if (vertical)
    {
        _attenuations[TYTypeAttenuation::DZ_TOP] = Dz;
        _attenuations[TYTypeAttenuation::ATTENUATION_BAR_TOP] = Dz;
        OSpectreOctave Agr = _attenuations[TYTypeAttenuation::ATTENUATION_GND_S] +
                             _attenuations[TYTypeAttenuation::ATTENUATION_GND_M] +
                             _attenuations[TYTypeAttenuation::ATTENUATION_GND_R];
        for (unsigned int i = 0; i < TY_SPECTRE_OCT_NB_ELMT; i++)
        {
            if (Agr.getTabValReel()[i] > 0)
            {
                _attenuations[TYTypeAttenuation::ATTENUATION_BAR_TOP].getTabValReel()[i] -=
                    Agr.getTabValReel()[i];
            }
            if (_attenuations[TYTypeAttenuation::ATTENUATION_BAR_TOP].getTabValReel()[i] < 0)
            {
                _attenuations[TYTypeAttenuation::ATTENUATION_BAR_TOP].getTabValReel()[i] = 0;
            }
        }
    }
    else
    {
        if (left)
        {
            _attenuations[TYTypeAttenuation::DZ_LEFT] = Dz;
            _attenuations[TYTypeAttenuation::ATTENUATION_BAR_LEFT] = Dz;
        }
        else
        {
            _attenuations[TYTypeAttenuation::DZ_RIGHT] = Dz;
            _attenuations[TYTypeAttenuation::ATTENUATION_BAR_RIGHT] = Dz;
        }
    }
}

OSpectreOctave& TYChemin::getAttenuation(TYTypeAttenuation type)
{
    return _attenuations[type];
}

void TYChemin::setAttenuationBarWhenNoPath(bool vertical, bool left)
{
    if (vertical)
    {
        _attenuations[TYTypeAttenuation::ATTENUATION_BAR_TOP] = OSpectreOctave{100.0};
    }
    else
    {
        if (left)
        {
            _attenuations[TYTypeAttenuation::ATTENUATION_BAR_LEFT] = OSpectreOctave{100.0};
        }
        else
        {
            _attenuations[TYTypeAttenuation::ATTENUATION_BAR_RIGHT] = OSpectreOctave{100.0};
        }
    }
}

void TYChemin::build_eq_path(const TYTabEtape& tabEtapes)
{

    for (size_t i = 0; i < tabEtapes.size(); i++)
    {
        _eq_path->addEvent(tabEtapes[i].asEvent());
    }
}

acoustic_path* TYChemin::get_ray(OPoint3D ptR)
{
    acoustic_event* receptor_event = new acoustic_event();
    receptor_event->pos = ptR;
    receptor_event->type = TYRECEPTEUR;
    _eq_path->addEvent(receptor_event);
    return _eq_path;
}

void TYChemin::calcGroundAttenuations(double dp, double hs, double hr, double Gs, double Gm, double Gr)
{
    // Main method for flat ground or with constant slope
    // Compute zones dimensions
    bool bHasIntermediateZone = dp > 30 * (hs + hr);

    double q = 0.0;
    if (bHasIntermediateZone)
    {
        q = 1 - 30 * (hs + hr) / dp;
    }

    // Compute ground attenuations for source, receptor and middle zones
    _attenuations[TYTypeAttenuation::ATTENUATION_GND_S] = calcGroundAttenuationSR(dp, hs, Gs);
    _attenuations[TYTypeAttenuation::ATTENUATION_GND_R] = calcGroundAttenuationSR(dp, hr, Gr);
    _attenuations[TYTypeAttenuation::ATTENUATION_GND_M] = calcGroundAttenuationM(q, Gm);
}

OSpectreOctave TYChemin::calcGroundAttenuationSR(double dp, double h, double G)
{
    double a = 1.5 + 3.0 * exp(-0.12 * (h - 5) * (h - 5)) * (1 - exp(-dp / 50.0)) +
               5.7 * exp(-0.09 * h * h) * (1 - exp(-2.8 * pow(10, -6) * dp * dp));
    double b = 1.5 + 8.6 * exp(-0.09 * h * h) * (1 - exp(-dp / 50.0));
    double c = 1.5 + 14.0 * exp(-0.46 * h * h) * (1 - exp(-dp / 50.0));
    double d = 1.5 + 5.0 * exp(-0.9 * h * h) * (1 - exp(-dp / 50.0));

    const double As[9]{-1.5,         -1.5,           -1.5 + G * a,   -1.5 + G * b,  -1.5 + G * c,
                       -1.5 + G * d, -1.5 * (1 - G), -1.5 * (1 - G), -1.5 * (1 - G)};
    return OSpectreOctave{As, 9, 0};
}

OSpectreOctave TYChemin::calcGroundAttenuationM(double q, double Gm)
{
    OSpectreOctave Agr_m = OSpectreOctave{0.0};
    if (q != 0.0)
    {
        const double Am[9]{-3.0 * q,          -3.0 * q,          -3 * q * (1 - Gm),
                           -3 * q * (1 - Gm), -3 * q * (1 - Gm), -3 * q * (1 - Gm),
                           -3 * q * (1 - Gm), -3 * q * (1 - Gm), -3 * q * (1 - Gm)};
        Agr_m = OSpectreOctave{Am, 9, 0};
    }
    return Agr_m;
}

::std::ostream& operator<<(::std::ostream& out, const TYTypeChemin& value)
{
    static std::map<TYTypeChemin, std::string> strings;
    if (strings.size() == 0)
    {
        strings[TYTypeChemin::CHEMIN_DIRECT] = "DIRECT";
        strings[TYTypeChemin::CHEMIN_SOL] = "SOL";
        strings[TYTypeChemin::CHEMIN_ECRAN] = "ECRAN";
        strings[TYTypeChemin::CHEMIN_REFLEX] = "REFLEX";
    }

    return out << strings[value];
}
