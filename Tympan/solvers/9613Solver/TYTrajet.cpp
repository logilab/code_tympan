/*
 * Copyright (C) <2012> <EDF-R&D> <FRANCE>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "TYTrajet.h"
#include "Tympan/models/solver/config.h"

TYTrajet::TYTrajet(tympan::AcousticSource& asrc_, tympan::AcousticReceptor& arcpt_)
    : asrc(asrc_), arcpt(arcpt_), _distance(0.0)
{
    _ptS = asrc.position;
    _ptR = arcpt.position;
    _distance = _ptS.distFrom(_ptR);
    // build_tab_rays();
}

TYTrajet::TYTrajet(const TYTrajet& other) : asrc(other.asrc), arcpt(other.arcpt)
{
    *this = other;
    if (tympan::SolverConfiguration::get()->KeepRays == false)
    {
        for (unsigned int i = 0; i < _tabRays.size(); i++)
        {
            delete _tabRays.at(i);
            _tabRays.at(i) = nullptr;
        }
        _tabRays.clear();
    }
}

TYTrajet::~TYTrajet()
{
    reset();
}

void TYTrajet::reset()
{
    _chemins.clear();
    _cheminsDirect.clear();
}

TYTrajet& TYTrajet::operator=(const TYTrajet& other)
{
    if (this != &other)
    {
        _chemins = other._chemins;
        _ptS = other._ptS;
        _ptR = other._ptR;
        _distance = other._distance;
        _sLP = other._sLP;
        asrc = other.asrc;
        arcpt = other.arcpt;
        asrc_idx = other.asrc_idx;
        arcpt_idx = other.arcpt_idx;
    }
    return *this;
}

bool TYTrajet::operator==(const TYTrajet& other) const
{
    if (this != &other)
    {
        if (_chemins != other._chemins)
        {
            return false;
        }
        if (_ptS != other._ptS)
        {
            return false;
        }
        if (_ptR != other._ptR)
        {
            return false;
        }
        if (_distance != other._distance)
        {
            return false;
        }
        if (_sLP != other._sLP)
        {
            return false;
        };
        // if (asrc != other.asrc) { return false; };
        // if (arcpt != other.arcpt) ;
        if (asrc_idx != other.asrc_idx)
        {
            return false;
        };
        if (arcpt_idx != other.arcpt_idx)
        {
            return false;
        };
    }

    return true;
}

bool TYTrajet::operator!=(const TYTrajet& other) const
{
    return !operator==(other);
}

void TYTrajet::addChemin(const TYChemin& chemin)
{
    _chemins.push_back(chemin);
}

void TYTrajet::addCheminDirect(const TYChemin& chemin)
{
    _cheminsDirect.push_back(chemin);
}

OSpectreOctave TYTrajet::getPNoOp()
{
    return _chemins[0].getAttenuation();
}

OSpectreOctave TYTrajet::getPEnergetique(const AtmosphericConditions& atmos)
{
    OSpectreOctave s = OSpectreOctave::getEmptyLinSpectre();
    OSpectreOctave sTemp;
    int firstReflex = -1;
    unsigned int indiceDebutEffetEcran = 0;
    unsigned int i;

    // On calcule l'attenuation sur le trajet direct (sauf chemins reflechis).
    for (i = 0; i < this->_chemins.size(); i++)
    {
        // Si un ecran est present, on ne traite pas les reflexions (dans un premier temp ...)
        if ((_chemins[0].getType() == TYTypeChemin::CHEMIN_ECRAN) &&
            (_chemins[i].getType() == TYTypeChemin::CHEMIN_REFLEX))
        {
            firstReflex = i;
            break;
        }
        sTemp = _chemins[i].getAttenuation();
        s = s + sTemp * sTemp; // somme des carres des modules
    }

    // Dans le cas d'un ecran, on compare l'attenuation obtenue a celle du trajet direct
    // pour eviter les effets d'amplification (plus de bruit avec l'ecran que sans ecran ...)
    if (_chemins[0].getType() == TYTypeChemin::CHEMIN_ECRAN)
    {
        OSpectreOctave attDirect = OSpectreOctave::getEmptyLinSpectre();

        for (i = 0; i < _cheminsDirect.size(); i++)
        {
            sTemp = _cheminsDirect[i].getAttenuation();
            attDirect = attDirect + sTemp * sTemp;
        }

        // On regarde l'attenuation globale obtenue pour chaque frequence,
        // on la compare a celle obtenue sur le trajet sans ecran,
        // si elle est superieure a 1 alors on prend la valeur obtenue pour le trajet sans ecran
        for (i = 0; i < s.getNbValues(); i++)
        {
            if (s.getTabValReel()[i] < attDirect.getTabValReel()[i])
            {
                indiceDebutEffetEcran = i; // On prend note de l'indice
                break; // Si l'ecran commence a attenuer plus que le trajet direct, il faut sortir de la
                       // boucle
            }
        } //*/

        if (firstReflex != -1) // S'il y a une reflexion sur un ecran
        {
            // On rajoute la contribution des chemins reflechis
            // 1. Aux chemins normaux et aux chemins directs
            for (i = firstReflex; i < _chemins.size(); i++)
            {
                sTemp = _chemins[i].getAttenuation() * _chemins[i].getAttenuation();
                s = s + sTemp;
                attDirect = attDirect + sTemp;
            }

            // On remplace la contribution du trajet direct pour toutes les frequences ou cela est necessaire
            for (i = 0; i < indiceDebutEffetEcran; i++)
            {
                s.getTabValReel()[i] = attDirect.getTabValReel()[i];
            }
        }
    }
    build_tab_rays();
    return s;
}

void TYTrajet::build_tab_rays()
{
    _tabRays.clear();
    for (size_t i = 0; i < _chemins.size(); i++)
    {
        _tabRays.push_back(_chemins[i].get_ray(_ptR));
    }
}

std::vector<acoustic_path*>& TYTrajet::get_tab_rays()
{
    return _tabRays;
}