/*
 * Copyright (C) <2012> <EDF-R&D> <FRANCE>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <solvers/9613Solver/TYSolverHelper.h>
#include <QtCore/qdir.h>
#include <fstream>

const std::vector<double> tabFreq = {31.5, 63.0, 125.0, 250.0, 500.0, 1000.0, 2000.0, 4000.0, 8000.0};
const std::vector<double> dAWeighting = {-39.5, -26.2, -16.1, -8.6, -3.2, 0.0, 1.2, 1.0, -1.1};
const char* SEP = ";";
const std::streamsize PRECISION = 2;

void TYSolverHelper::exportResults17534(const std::vector<PathResults> pathsResults,
                                        const OSpectreOctave& SLp, const AtmosphericConditions& atmos)
{
    QString qFileName =
        QDir::toNativeSeparators("C:/projects/tympan_tools/17534-3/output/results_17534_format.csv");
    std::ofstream ofs;
    ofs.open(qFileName.toStdString().c_str(), std::ios_base::out);
    if (ofs.is_open())
    {
        ofs.setf(std::ios::fixed, std::ios::floatfield);
        ofs.precision(PRECISION);

        for (auto& currentPathResults : pathsResults)
        {
            ofs << "Path id : " << SEP << currentPathResults.path_id << "\n";
            ofs << "Path type : " << SEP << currentPathResults.pathType << "\n";
            // Array titles
            ofs << "Quantity" << SEP << "Unit" << SEP << "Values" << SEP << SEP << SEP << SEP << SEP << SEP
                << SEP << SEP << "\n";

            // Frequencies
            ofs << "f" << SEP << "Hz" << SEP;
            for (int col = 0; col < tabFreq.size(); col++)
            {
                ofs << tabFreq[col] << SEP;
            }
            ofs << std::endl;

            // Lw
            printQuantity(ofs, currentPathResults.LW, "Lw");

            // Dc
            printQuantity(ofs, currentPathResults.Dc, "Dc");

            // Atmospherical attenuation coefficient
            ofs << "alpha-atm" << SEP << "dB/km" << SEP;
            for (int col = 0; col < tabFreq.size(); col++)
            {
                ofs << atmos.get_absorption_spectrum_oct().getTabValReel()[col] << SEP;
            }
            ofs << std::endl;

            // Atmospherical attenuation
            printQuantity(ofs, currentPathResults.Aatm, "Aatm");

            // Ground attenuations
            printQuantity(ofs, currentPathResults.Agr_s, "Agr_s");
            printQuantity(ofs, currentPathResults.Agr_r, "Agr_r");
            printQuantity(ofs, currentPathResults.Agr_m, "Agr_m");
            printQuantity(ofs, currentPathResults.Agr_s + currentPathResults.Agr_r + currentPathResults.Agr_m,
                          "Agr");

            // Barrier attenuations
            printQuantity(ofs, currentPathResults.Dz_top, "Dz_top");
            printQuantity(ofs, currentPathResults.Dz_left, "Dz_left");
            printQuantity(ofs, currentPathResults.Dz_right, "Dz_right");

            printQuantity(ofs, currentPathResults.Abar_top, "Abar_top");
            printQuantity(ofs, currentPathResults.Abar_left, "Abar_left");
            printQuantity(ofs, currentPathResults.Abar_right, "Abar_right");
            printQuantity(ofs, currentPathResults.Abar, "Abar");

            // Geometrical spreading attenuation
            printQuantity(ofs, currentPathResults.Adiv, "Adiv");

            // Level
            printLevel(ofs, currentPathResults.L, "L");
            ofs << std::endl;
        }

        // Print global result sum of paths results
        printLevel(ofs, SLp, "Lglobal");
        ofs << std::endl;

        ofs.close();
    }
}

void TYSolverHelper::printQuantity(std::ofstream& ofs, const OSpectreOctave Att, const char* quantity)
{
    ofs << quantity << SEP << "dB" << SEP;
    for (int col = 0; col < tabFreq.size(); col++)
    {
        ofs << Att.getTabValReel()[col] << SEP;
    }
    ofs << std::endl;
}

void TYSolverHelper::printLevel(std::ofstream& ofs, const OSpectreOctave L, const char* quantity)
{
    // Level
    ofs << quantity << SEP << "dB" << SEP;
    for (int col = 0; col < tabFreq.size(); col++)
    {
        ofs << L.getTabValReel()[col] << SEP;
    }
    ofs << L.valGlobDBLin() << SEP;
    ofs << std::endl;

    // A-weighting
    ofs << "A-weighting" << SEP << "dB" << SEP;
    for (int col = 0; col < tabFreq.size(); col++)
    {
        ofs << dAWeighting[col] << SEP;
    }
    ofs << std::endl;

    // A-Level
    ofs << quantity << "A" << SEP << "dBA" << SEP;
    for (int col = 0; col < tabFreq.size(); col++)
    {
        ofs << L.getTabValReel()[col] + dAWeighting[col] << SEP;
    }
    ofs << L.valGlobDBA() << SEP;
    ofs << std::endl;
}
