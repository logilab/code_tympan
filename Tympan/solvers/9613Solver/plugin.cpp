/*
 * Copyright (C) <2012-2014> <EDF-R&D> <FRANCE>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * \brief Le plugin associe au solver 9613
 */

#include "Tympan/core/plugin.h"
#include "Tympan/solvers/9613Solver/TYSolver.h"

Plugin* plugin;

extern "C" PLUGIN_DECL void startPlugin()
{
    plugin = new Plugin();
    // Information sur le plugin
    plugin->setName("9613Solver");
    plugin->setAuthor("Projet_Tympan");
    plugin->setVersion("1.0");
    plugin->setDescription("Solveur acoustique conforme a la norme 9613");
    plugin->setUUID(OGenID("{B45873B6-550C-11ED-BDC3-0242AC120002}"));
    // Creation du solver
    plugin->setSolver(new TYSolver());
}

extern "C" PLUGIN_DECL Plugin* getPlugin()
{
    return plugin;
}

extern "C" PLUGIN_DECL void stopPlugin()
{
    delete plugin;
}
