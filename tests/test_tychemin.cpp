/**
 *
 * @brief Functional tests of the TYChemin class
 *
 *  Created on: january 18, 2018
 *  Author: Philippe CHAPUIS <philippe.chapuis@c-s.fr>
 *
 */
#include "gtest/gtest.h"
#include "Tympan/solvers/DefaultSolver/TYChemin.h"
#include "Tympan/models/solver/entities.hpp"
#include "Tympan/solvers/DefaultSolver/TYSolver.h"
#include "Tympan/models/solver/acoustic_problem_model.hpp"
#include "Tympan/models/solver/data_model_common.hpp"
#include "Tympan/solvers/DefaultSolver/TYAcousticModel.h"
#include <iostream>
#include "testutils.h"

/**
 * @brief Fixture which provides an instance of TYChemin
 */
class TYCheminTest : public testing::Test
{

public:
};

// Testing the TYChemin::calcAttenuation method
TEST_F(TYCheminTest, calcAttenuation)
{
    // initialisation global objects
    TYInitUserCase initCase;
    initCase.initGlobal(600.0);

    // variable condition atmospherique
    AtmosphericConditions _atmos_test(101325.0, 20.0, 50.0);
    OSpectre _expectedSpectre;
    OSpectre _returnSpectreDB;

    // CAS 1 CHEMIN DIRECT
    _expectedSpectre = initCase.initDirectPathWay();

    // vérification du nombre d'étapes dans le tableau
    EXPECT_EQ(1, initCase.m_tabEtape01.size());

    // appel de la méthode à tester
    initCase.m_chemin_direct.calcAttenuation(initCase.m_tabEtape01, _atmos_test);

    // comparaison des spectre
    _returnSpectreDB = initCase.m_chemin_direct.getAttenuation().toDB();

    // vérification de la comparaison
    // TODO Mettre à jour les spectres attendus avec la calculette 03_détails_calcul_acoustique.xlsx
    EXPECT_FALSE(_expectedSpectre == _returnSpectreDB);

    // end cas 1

    // CAS 2 CHEMIN SOL
    _expectedSpectre = initCase.initGroundPathWay();

    // vérification du nombre d'étapes dans le tableau
    EXPECT_EQ(2, initCase.m_tabEtape01.size());

    // appel de la méthode à tester
    initCase.m_chemin_sol.calcAttenuation(initCase.m_tabEtape01, _atmos_test);

    // comparaison des spectre
    _returnSpectreDB = initCase.m_chemin_sol.getAttenuation().toDB();

    // vérification de la comparaison
    // TODO Mettre à jour les spectres attendus avec la calculette 03_détails_calcul_acoustique.xlsx
    EXPECT_FALSE(_expectedSpectre == _returnSpectreDB);

    // end cas 2

    // CAS 3 CHEMIN ECRAN
    _expectedSpectre = initCase.initBlockPathWay();

    // vérification du nombre d'étapes dans le tableau
    EXPECT_EQ(4, initCase.m_tabEtape01.size());

    // appel de la méthode à tester
    initCase.m_chemin_ecran.calcAttenuation(initCase.m_tabEtape01, _atmos_test);

    // comparaison des spectre
    _returnSpectreDB = initCase.m_chemin_ecran.getAttenuation().toDB();

    // vérification de la comparaison
    // TODO Mettre à jour les spectres attendus avec la calculette 03_détails_calcul_acoustique.xlsx
    EXPECT_FALSE(_expectedSpectre == _returnSpectreDB);

    // end cas 3

    // CAS 4 CHEMIN REFLEX
    _expectedSpectre = initCase.initReflexionPathWay();

    // vérification du nombre d'étapes dans le tableau
    EXPECT_EQ(2, initCase.m_tabEtape01.size());

    // appel de la méthode ? tester
    initCase.m_chemin_reflex.calcAttenuation(initCase.m_tabEtape01, _atmos_test);

    // comparaison des spectre
    _returnSpectreDB = initCase.m_chemin_reflex.getAttenuation().toDB();

    // vérification de la comparaison
    // TODO Mettre à jour les spectres attendus avec la calculette 03_détails_calcul_acoustique.xlsx
    EXPECT_FALSE(_expectedSpectre == _returnSpectreDB);

    // end cas 4
}
