#include "gtest/gtest.h"

#include <cstdlib>
#include <time.h>

#include <QCoreApplication>
#include <QJsonDocument>
#include <QJsonParseError>
#include <QJsonObject>
#include <QApplication>
#include <QSignalSpy>

#include "Tympan/models/business/OLocalizator.h"
#include "Tympan/core/config.h"
#include "Tympan/core/exceptions.h"
#include "TympanTestsConfig.h"
#include "Tympan/gui/widgets/TYSolverParamsWidgetManager.h"

#include <iostream>
#include <fstream>

using std::cerr;
using std::cout;
using std::endl;

struct test_SolverParams : public ::testing::Test
{
    // setup the TYSolverParamsWidgetManager
    void SetUp() override
    {
        int argc = 0;
        char** argv = new char*[0];

        if (QCoreApplication::instance() == nullptr)
            app_ = new QApplication(argc, argv);

        OLocalizator::setRessourceFile(QString(TYMPAN_ROOT_DIR "/resources/Language_EN.xml"));
        OLocalizator::setRessourceFile(QString(TYMPAN_ROOT_DIR "/resources/Language_FR.xml"));
        OLocalizator::setRessourcePath(QString(TYMPAN_ROOT_DIR "resources"));

        dataModelPath_ = QString(tympan::path_to_test_data("solver_config_datamodel.json").c_str());
        dataModelPath_ = QDir::toNativeSeparators(dataModelPath_);
        solverParamsWidgetManager_ = new TYSolverParamsWidgetManager(dataModelPath_);
    }
    QCoreApplication* app_;
    QString dataModelPath_;
    TYSolverParamsWidgetManager* solverParamsWidgetManager_;
};

// Check that loading a non existing data model raises an error
TEST_F(test_SolverParams, load_non_existing_data_model_file)
{
    QString nonExistingdataModelPath(TYMPAN_ROOT_DIR "/resources/nonexisting_solver_config_datamodel.json");
    nonExistingdataModelPath = QDir::toNativeSeparators(nonExistingdataModelPath);
    EXPECT_THROW(new TYSolverParamsWidgetManager(nonExistingdataModelPath), tympan::invalid_data);
}

// Check that loading an existing data model does not fail
TEST_F(test_SolverParams, load_existing_data_model_file)
{
    QString dataModelPath = QDir(TYMPAN_ROOT_DIR).absoluteFilePath(SOLVER_PARAMS_JSON);
    EXPECT_NO_FATAL_FAILURE(new TYSolverParamsWidgetManager(dataModelPath));
}

// Check that recovering a non existing data model raises an error
TEST_F(test_SolverParams, recover_non_exisisting_data_model)
{
    EXPECT_THROW(solverParamsWidgetManager_->getDataModel("nonExistingParam"), tympan::invalid_data);
}

// Check that recovering an existing data model does not raise an error
// and that the recovered data model is correct
TEST_F(test_SolverParams, recover_exisisting_data_model)
{
    TYSolverParamsDataModel* dataModel;

    EXPECT_NO_THROW(dataModel = solverParamsWidgetManager_->getDataModel("PropaConditions"));

    EXPECT_EQ(dataModel->paramName.toStdString(), "PropaConditions");
    EXPECT_EQ(dataModel->type.toStdString(), "int");
    EXPECT_EQ(dataModel->valueLabels, QStringList() << "non_refracting"
                                                    << "downward"
                                                    << "mixed");

    EXPECT_TRUE(dataModel->isLabeledValues());
    EXPECT_TRUE(dataModel->isInt());
    EXPECT_TRUE(dataModel->isNumber());
    EXPECT_FALSE(dataModel->isBool());
}

// Check the correctness of a data model created by hand
TEST_F(test_SolverParams, create_data_model)
{

    QStringList labels = QStringList() << "l1"
                                       << "l2"
                                       << "l3"
                                       << "l4";
    TYSolverParamsDataModel* dataModel = new TYSolverParamsDataModel("labels", "int", labels);

    EXPECT_EQ(dataModel->paramName.toStdString(), "labels");
    EXPECT_EQ(dataModel->type.toStdString(), "int");
    EXPECT_EQ(dataModel->defaultValue.toStdString(), "0");
    EXPECT_EQ(dataModel->valueLabels, labels);

    EXPECT_TRUE(dataModel->isLabeledValues());
    EXPECT_TRUE(dataModel->isInt());
    EXPECT_TRUE(dataModel->isNumber());
    EXPECT_FALSE(dataModel->isBool());
}

// Test the TYSolverParamsDataModel::isLabeledValues is correct
TEST_F(test_SolverParams, test_isLabeledValues)
{
    QStringList labels = QStringList() << "l1"
                                       << "l2"
                                       << "l3"
                                       << "l4";
    TYSolverParamsDataModel* dataModel = new TYSolverParamsDataModel("labelsDataModel", "int", labels);
    EXPECT_TRUE(dataModel->isLabeledValues());

    dataModel = new TYSolverParamsDataModel("labelsDataModel", "bool", labels);
    EXPECT_TRUE(dataModel->isLabeledValues());

    dataModel = new TYSolverParamsDataModel("labelsDataModel", "float", labels);
    EXPECT_TRUE(dataModel->isLabeledValues());

    dataModel = new TYSolverParamsDataModel("labelsDataModel", "double", labels);
    EXPECT_TRUE(dataModel->isLabeledValues());

    dataModel = new TYSolverParamsDataModel("labelsDataModel", "int", QStringList());
    EXPECT_FALSE(dataModel->isLabeledValues());

    dataModel = new TYSolverParamsDataModel("labelsDataModel", "bool", QStringList());
    EXPECT_FALSE(dataModel->isLabeledValues());

    dataModel = new TYSolverParamsDataModel("labelsDataModel", "float", QStringList());
    EXPECT_FALSE(dataModel->isLabeledValues());

    dataModel = new TYSolverParamsDataModel("labelsDataModel", "double", QStringList());
    EXPECT_FALSE(dataModel->isLabeledValues());
}

// Test the TYSolverParamsDataModel::isInt is correct
TEST_F(test_SolverParams, test_isInt)
{
    TYSolverParamsDataModel* dataModel = new TYSolverParamsDataModel("intDataModel", "int");
    EXPECT_TRUE(dataModel->isInt());

    dataModel = new TYSolverParamsDataModel("intDataModel", "bool");
    EXPECT_FALSE(dataModel->isInt());

    dataModel = new TYSolverParamsDataModel("intDataModel", "float");
    EXPECT_FALSE(dataModel->isInt());

    dataModel = new TYSolverParamsDataModel("intDataModel", "double");
    EXPECT_FALSE(dataModel->isInt());
}

// Test the TYSolverParamsDataModel::isBool is correct
TEST_F(test_SolverParams, test_isBool)
{
    QStringList labels = QStringList() << "l1"
                                       << "l2"
                                       << "l3"
                                       << "l4";
    TYSolverParamsDataModel* dataModel = new TYSolverParamsDataModel("boolDataModel", "bool");
    EXPECT_TRUE(dataModel->isBool());

    dataModel = new TYSolverParamsDataModel("boolDataModel", "int");
    EXPECT_FALSE(dataModel->isBool());

    dataModel = new TYSolverParamsDataModel("boolDataModel", "float");
    EXPECT_FALSE(dataModel->isBool());

    dataModel = new TYSolverParamsDataModel("boolDataModel", "double");
    EXPECT_FALSE(dataModel->isBool());
}

// Test the TYSolverParamsDataModel::isNumber is correct
TEST_F(test_SolverParams, test_isNumber)
{
    QStringList labels = QStringList() << "l1"
                                       << "l2"
                                       << "l3"
                                       << "l4";
    TYSolverParamsDataModel* dataModel = new TYSolverParamsDataModel("numberDataModel", "bool");
    EXPECT_FALSE(dataModel->isNumber());

    dataModel = new TYSolverParamsDataModel("numberDataModel", "int");
    EXPECT_TRUE(dataModel->isNumber());

    dataModel = new TYSolverParamsDataModel("numberDataModel", "float");
    EXPECT_TRUE(dataModel->isNumber());

    dataModel = new TYSolverParamsDataModel("numberDataModel", "double");
    EXPECT_TRUE(dataModel->isNumber());
}

// Check that recovering a non existing widget raises an error
TEST_F(test_SolverParams, recover_non_exisisting_widget)
{
    EXPECT_THROW(solverParamsWidgetManager_->getWidget("nonExistingWidget"), tympan::invalid_data);
}

// Check that creating and then recovering a widget does not raise an error
TEST_F(test_SolverParams, create_and_recover_widget)
{

    // Create data pmodel and make widget
    TYSolverParamsDataModel* dataModel;
    EXPECT_NO_THROW(dataModel = solverParamsWidgetManager_->getDataModel("PropaConditions"));
    EXPECT_NO_THROW(solverParamsWidgetManager_->makeWidget("PropaConditions"));

    // Recover widget
    TYSolverParamsWidget* widget;
    EXPECT_NO_THROW(widget = solverParamsWidgetManager_->getWidget("PropaConditions"));
    EXPECT_EQ(dataModel, widget->dataModel);
}

// Check that the TYSolverParamsRadioButtonsWidget widget works properly
TEST_F(test_SolverParams, test_radio_button_widget)
{

    // initialise a data model with labels
    TYSolverParamsDataModel* dataModel = new TYSolverParamsDataModel("labels", "int",
                                                                     QStringList() << "l1"
                                                                                   << "l2"
                                                                                   << "l3"
                                                                                   << "l4");

    // Create and recover widget
    EXPECT_NO_THROW(solverParamsWidgetManager_->makeWidget(dataModel));
    TYSolverParamsWidget* widget;
    EXPECT_NO_THROW(widget = solverParamsWidgetManager_->getWidget("labels"));

    // Check that the widget is a TYSolverParamsRadioButtonsWidget
    TYSolverParamsRadioButtonsWidget* radioButtonWidget =
        dynamic_cast<TYSolverParamsRadioButtonsWidget*>(widget);
    EXPECT_TRUE(radioButtonWidget != nullptr);

    // Spy on valueChanged signal
    QSignalSpy spy(widget, SIGNAL(valueChanged(QString)));
    EXPECT_TRUE(spy.isValid());
    EXPECT_EQ(spy.count(), 0);

    QButtonGroup* buttonGroup = radioButtonWidget->buttonGroup;

    // Default value is 0
    EXPECT_EQ(widget->value().toStdString(), "0");

    // Modifie the widget state and check that the value is properly updated
    buttonGroup->button(0)->click();
    EXPECT_EQ(widget->value().toStdString(), "0");
    EXPECT_EQ(spy.count(), 0); // the signal should not be emitted since the value has not been modified

    buttonGroup->button(1)->click();
    EXPECT_EQ(widget->value().toStdString(), "1");
    EXPECT_EQ(spy.count(), 1);

    buttonGroup->button(2)->click();
    EXPECT_EQ(widget->value().toStdString(), "2");
    EXPECT_EQ(spy.count(), 2);

    buttonGroup->button(3)->click();
    EXPECT_EQ(widget->value().toStdString(), "3");
    EXPECT_EQ(spy.count(), 3);

    // Modifie the value and check that the widget correctly reflects the change
    widget->setValue(0);
    EXPECT_EQ(buttonGroup->checkedId(), 0);
    EXPECT_EQ(spy.count(), 4);

    widget->setValue(1);
    EXPECT_EQ(buttonGroup->checkedId(), 1);
    EXPECT_EQ(spy.count(), 5);

    widget->setValue(2);
    EXPECT_EQ(buttonGroup->checkedId(), 2);
    EXPECT_EQ(spy.count(), 6);

    widget->setValue(3);
    EXPECT_EQ(buttonGroup->checkedId(), 3);
    EXPECT_EQ(spy.count(), 7);
}

// Check that the TYSolverParamsCheckBoxWidget widget works properly
TEST_F(test_SolverParams, test_check_box_widget)
{

    // initialise a boolean
    TYSolverParamsDataModel* dataModel = new TYSolverParamsDataModel("checkbox", "bool");

    // Create and recover widget
    EXPECT_NO_THROW(solverParamsWidgetManager_->makeWidget(dataModel));
    TYSolverParamsWidget* widget;
    EXPECT_NO_THROW(widget = solverParamsWidgetManager_->getWidget("checkbox"));

    // Check that the widget is a TYSolverParamsCheckBoxWidget
    TYSolverParamsCheckBoxWidget* checkBoxWidget = dynamic_cast<TYSolverParamsCheckBoxWidget*>(widget);
    EXPECT_TRUE(checkBoxWidget != nullptr);

    QCheckBox* checkBox = checkBoxWidget->checkBox;

    // Spy on valueChanged signal
    QSignalSpy spy(widget, SIGNAL(valueChanged(QString)));
    EXPECT_TRUE(spy.isValid());
    EXPECT_EQ(spy.count(), 0);

    // Default value is 0
    EXPECT_EQ(widget->value().toStdString(), "0");

    // Modifie the checkbox state and check that the value is properly updated
    checkBox->setChecked(false);
    EXPECT_EQ(widget->value().toStdString(), "0");
    EXPECT_EQ(spy.count(), 0); // the signal should not be emitted since the value has not been modified

    checkBox->setChecked(true);
    EXPECT_EQ(widget->value().toStdString(), "1");
    EXPECT_EQ(spy.count(), 1);

    // Modifie the value and check that the checkbox correctly reflects the change
    widget->setValue(false);
    EXPECT_FALSE(checkBox->isChecked());
    EXPECT_EQ(spy.count(), 2);

    widget->setValue(true);
    EXPECT_TRUE(checkBox->isChecked());
    EXPECT_EQ(spy.count(), 3);
}

// Check that the TYSolverParamsInputValueWidget widget works properly
TEST_F(test_SolverParams, test_input_value_widget)
{
    // initialise a numerical data model
    TYSolverParamsDataModel* dataModel = new TYSolverParamsDataModel("input", "float");

    // Create and recover widget
    EXPECT_NO_THROW(solverParamsWidgetManager_->makeWidget(dataModel));
    TYSolverParamsWidget* widget;
    EXPECT_NO_THROW(widget = solverParamsWidgetManager_->getWidget("input"));

    // Check that the widget is a TYSolverParamsInputValueWidget
    TYSolverParamsInputValueWidget* inputValueWidget = dynamic_cast<TYSolverParamsInputValueWidget*>(widget);
    EXPECT_TRUE(inputValueWidget != nullptr);

    QLineEdit* lineEdit = inputValueWidget->lineEdit;

    // Spy on valueChanged signal
    QSignalSpy spy(widget, SIGNAL(valueChanged(QString)));
    EXPECT_TRUE(spy.isValid());
    EXPECT_EQ(spy.count(), 0);

    // Default value is 0
    EXPECT_EQ(widget->value().toStdString(), "0");

    // Modifie the lineEdit and check that the value is properly updated
    lineEdit->setText("0");
    EXPECT_EQ(widget->value().toStdString(), "0");
    EXPECT_EQ(spy.count(), 0); // the signal should not be emitted since the value has not been modified

    lineEdit->setText("0.001");
    EXPECT_EQ(widget->value().toStdString(), "0.001");
    EXPECT_EQ(spy.count(), 1);

    lineEdit->setText("-10");
    EXPECT_EQ(widget->value().toStdString(), "-10");
    EXPECT_EQ(spy.count(), 2);

    // Modifie the value and check that the checkbox correctly reflects the change
    widget->setValue(QString("35.56"));
    EXPECT_EQ(lineEdit->text().toStdString(), "35.56");
    EXPECT_EQ(spy.count(), 3);

    widget->setValue(QString("0"));
    EXPECT_EQ(lineEdit->text().toStdString(), "0");
    EXPECT_EQ(spy.count(), 4);
}

TEST_F(test_SolverParams, check_all_data_models)
{

    QFile file(dataModelPath_);
    bool opened = file.open(QIODevice::ReadOnly | QIODevice::Text);

    EXPECT_TRUE(opened);

    QJsonParseError JsonParseError;
    QJsonObject dataModel = QJsonDocument::fromJson(file.readAll(), &JsonParseError).object();
    file.close();

    // make the widgets of all parameters present in the datamodel.json file
    for (QString sectionName : dataModel.keys())
    {
        QJsonObject section = dataModel[sectionName].toObject();
        for (QString paramName : section.keys())
        {
            EXPECT_NO_THROW(solverParamsWidgetManager_->makeWidget(paramName));
        }
    }

    // check that data models and widgets agre with the datamodel.json file
    for (QString sectionName : dataModel.keys())
    {
        QJsonObject section = dataModel[sectionName].toObject();
        for (QString paramName : section.keys())
        {
            QJsonObject datamModelJson = section[paramName].toObject();
            TYSolverParamsDataModel* dataModel = solverParamsWidgetManager_->getDataModel(paramName);

            EXPECT_EQ(dataModel->paramName.toStdString(), paramName.toStdString());
            EXPECT_EQ(dataModel->type.toStdString(), datamModelJson["type"].toString().toStdString());
            EXPECT_EQ(dataModel->valueLabels, datamModelJson["labels"].toVariant().toStringList());

            QString defaultValue = datamModelJson["default"].toVariant().toString();
            if (datamModelJson["type"].toString() == "bool")
                if (defaultValue == "true")
                    defaultValue = "1";
                else if (defaultValue == "false")
                    defaultValue = "0";
            EXPECT_EQ(dataModel->defaultValue.toStdString(), defaultValue.toStdString());
        }
    }
}

// Create widgets for all parameters in the datamodel.json file and check their correctness
TEST_F(test_SolverParams, check_all_widgets)
{

    QFile file(dataModelPath_);
    bool opened = file.open(QIODevice::ReadOnly | QIODevice::Text);

    EXPECT_TRUE(opened);

    QJsonParseError JsonParseError;
    QJsonObject dataModel = QJsonDocument::fromJson(file.readAll(), &JsonParseError).object();
    file.close();

    // make widgets for all parameters present in the datamodel.json file
    for (QString sectionName : dataModel.keys())
    {
        QJsonObject section = dataModel[sectionName].toObject();
        for (QString paramName : section.keys())
        {
            EXPECT_NO_THROW(solverParamsWidgetManager_->makeWidget(paramName));
        }
    }

    // check that the widgets correspond to their datamodel
    for (QString sectionName : dataModel.keys())
    {
        QJsonObject section = dataModel[sectionName].toObject();
        for (QString paramName : section.keys())
        {
            TYSolverParamsDataModel* dataModel = solverParamsWidgetManager_->getDataModel(paramName);
            TYSolverParamsWidget* widget = solverParamsWidgetManager_->getWidget(paramName);

            EXPECT_EQ(dataModel, widget->dataModel);
            EXPECT_EQ(widget->value(), dataModel->defaultValue);
            if (dataModel->isLabeledValues())
                EXPECT_TRUE(dynamic_cast<TYSolverParamsRadioButtonsWidget*>(widget) != nullptr);
            else if (dataModel->isBool())
                EXPECT_TRUE(dynamic_cast<TYSolverParamsCheckBoxWidget*>(widget) != nullptr);
            else
                EXPECT_TRUE(dynamic_cast<TYSolverParamsInputValueWidget*>(widget) != nullptr);
        }
    }
}

// Check that the SolverParamsWidgetManager updates the widgets' states correctly when the parmeters change
// and conversly
TEST_F(test_SolverParams, test_solver_params_manager)
{
    // setup 3 different widgets
    solverParamsWidgetManager_->makeWidget(new TYSolverParamsDataModel("param1", "int"));
    solverParamsWidgetManager_->makeWidget(new TYSolverParamsDataModel("param2", "bool"));
    solverParamsWidgetManager_->makeWidget(new TYSolverParamsDataModel("param3", "int",
                                                                       QStringList() << "label1"
                                                                                     << "label2"
                                                                                     << "label3"));
    // update the parameters and check that the widgets return the correct values
    QString solverParams = "[SECTION]param1=10\nparam2=True\nparam3=1\n";
    solverParamsWidgetManager_->updateWidgets(solverParams);
    EXPECT_EQ(solverParamsWidgetManager_->getSolverParams().toStdString(), solverParams.toStdString());

    // modifie the values and check that the string returned by  SolverParamsWidgetManager::getSolverParams is
    // correct
    solverParamsWidgetManager_->getWidget("param1")->setValue(42);
    solverParamsWidgetManager_->getWidget("param2")->setValue(false);
    solverParamsWidgetManager_->getWidget("param3")->setValue(2);
    EXPECT_EQ(solverParamsWidgetManager_->getSolverParams().toStdString(),
              "[SECTION]param1=42\nparam2=False\nparam3=2\n");
}