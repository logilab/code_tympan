import unittest

from utils import TympanTC, _test_solve_with_file


class TestSolve9613_REFLEXION_TU19_17534(TympanTC):
    def test_9613_reflexion_TU19_17534(self):
        _test_solve_with_file("TEST_9613_REFLEXION_TU19_17534_NO_RESU.xml", self)


if __name__ == "__main__":
    unittest.main()
