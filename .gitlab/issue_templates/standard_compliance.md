**Story :**\
As Tympan stakeholder, I want to check Tympan compliance with ... standard for ..., in order to ... .

**Acceptance criteria :**
- Read standard documentation,
- Write functional requirements relative to this documentation,
- Test Tympan compliance with these requirements,
- Write deviation between Tympan behaviour and requirements.
